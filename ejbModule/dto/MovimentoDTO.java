package dto;

import java.io.Serializable;
import java.util.Date;

public class MovimentoDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer idMovimento;
	private Integer idCliente;
	private ContoCorrenteDTO contoCorrente;
	private String tipoMovimento;
	private Integer importo;
	private Date dataMovimento;

	public Integer getIdMovimento() {
		return idMovimento;
	}

	public void setIdMovimento(Integer idMovimento) {
		this.idMovimento = idMovimento;
	}

	public Integer getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(Integer idCliente) {
		this.idCliente = idCliente;
	}

	public ContoCorrenteDTO getContoCorrente() {
		return contoCorrente;
	}

	public void setContoCorrente(ContoCorrenteDTO contoCorrente) {
		this.contoCorrente = contoCorrente;
	}

	public String getTipoMovimento() {
		return tipoMovimento;
	}

	public void setTipoMovimento(String tipoMovimento) {
		this.tipoMovimento = tipoMovimento;
	}

	public Integer getImporto() {
		return importo;
	}

	public void setImporto(Integer importo) {
		this.importo = importo;
	}

	public Date getDataMovimento() {
		return dataMovimento;
	}

	public void setDataMovimento(Date dataMovimento) {
		this.dataMovimento = dataMovimento;
	}
}
