package dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class ContoCorrenteDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer idContoCorrente;
	private ClienteDTO cliente;
	private String iban;
	private Integer saldo;
	private Date dataCreazione;
	private List<MovimentoDTO> movimenti;

	public Integer getIdContoCorrente() {
		return idContoCorrente;
	}

	public void setIdContoCorrente(Integer idContoCorrente) {
		this.idContoCorrente = idContoCorrente;
	}

	public ClienteDTO getCliente() {
		return cliente;
	}

	public void setCliente(ClienteDTO cliente) {
		this.cliente = cliente;
	}

	public String getIban() {
		return iban;
	}

	public void setIban(String iban) {
		this.iban = iban;
	}

	public Integer getSaldo() {
		return saldo;
	}

	public void setSaldo(Integer saldo) {
		this.saldo = saldo;
	}

	public Date getDataCreazione() {
		return dataCreazione;
	}

	public void setDataCreazione(Date dataCreazione) {
		this.dataCreazione = dataCreazione;
	}

	public List<MovimentoDTO> getMovimenti() {
		return movimenti;
	}

	public void setMovimenti(List<MovimentoDTO> movimenti) {
		this.movimenti = movimenti;
	}
}
