package dto;

import java.io.Serializable;

public class ClienteDTO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer idCliente;

	private ContoCorrenteDTO contoCorrente;

	public Integer getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(Integer idCliente) {
		this.idCliente = idCliente;
	}

	public ContoCorrenteDTO getContoCorrente() {
		return contoCorrente;
	}

	public void setContoCorrente(ContoCorrenteDTO contoCorrente) {
		this.contoCorrente = contoCorrente;
	}

}
