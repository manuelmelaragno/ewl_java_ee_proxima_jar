package facade;

import java.sql.SQLException;
import java.util.List;

import javax.ejb.Remote;
import javax.naming.NamingException;

import dto.MovimentoDTO;

@Remote
public interface MovimentoEJBRemote {
	public boolean insertMovimento(Integer idContoCorrente, Integer importo) throws ClassNotFoundException, SQLException, NamingException;

	public List<MovimentoDTO> getMovimentiByIBAN(String iban) throws ClassNotFoundException, SQLException, NamingException;

	public List<MovimentoDTO> getMovimentiByIdConto(Integer idConto) throws ClassNotFoundException, SQLException, NamingException;

	public List<MovimentoDTO> getMovimenti() throws ClassNotFoundException, SQLException, NamingException;

	public void deleteMovimento(Integer idMovimento) throws ClassNotFoundException, SQLException, NamingException;

	public List<MovimentoDTO> getMovimentiByImporto(Integer importo);
}
