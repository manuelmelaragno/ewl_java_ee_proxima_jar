package facade;

import java.sql.SQLException;

import javax.ejb.Local;
import javax.naming.NamingException;

import dto.ClienteDTO;

@Local
public interface ClienteEJBLocal {
	public boolean insertCliente() throws ClassNotFoundException, SQLException, NamingException;

	public ClienteDTO getClienteById(Integer idCliente) throws ClassNotFoundException, SQLException, NamingException;
}
