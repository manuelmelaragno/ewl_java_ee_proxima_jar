package facade;

import java.sql.SQLException;

import javax.ejb.Remote;
import javax.naming.NamingException;

import dto.ClienteDTO;

@Remote
public interface ClienteEJBRemote {
	public boolean insertCliente() throws ClassNotFoundException, SQLException, NamingException;

	public ClienteDTO getClienteById(Integer idCliente) throws ClassNotFoundException, SQLException, NamingException;
}
