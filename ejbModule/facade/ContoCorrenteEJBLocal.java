package facade;

import java.sql.SQLException;
import java.util.List;

import javax.ejb.Local;
import javax.naming.NamingException;

import dto.ContoCorrenteDTO;

@Local
public interface ContoCorrenteEJBLocal {
	public boolean insertContoCorrente(Integer idCliente) throws ClassNotFoundException, SQLException, NamingException;

	public ContoCorrenteDTO getContoById(Integer idConto) throws SQLException, ClassNotFoundException, NamingException;

	public ContoCorrenteDTO getContoByIdCliente(Integer idCliente) throws SQLException, ClassNotFoundException, NamingException;

	public List<ContoCorrenteDTO> getContiCorrente() throws ClassNotFoundException, SQLException, NamingException;

	public ContoCorrenteDTO getContoByIban(String iban) throws SQLException, ClassNotFoundException, NamingException;

	public void deleteContoCorrente(Integer idConto) throws ClassNotFoundException, SQLException, NamingException;

	public ContoCorrenteDTO updateContoCorrente(ContoCorrenteDTO dtoConto);
}
